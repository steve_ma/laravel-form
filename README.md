# laravel-afs

#### 介绍
研究了一下 symfony/form 的源码 搞的有些太复杂了，一般情况下用不到这么写功能。写一个简单的form管理

### 安装教程

```shell

# 安装

$ composer require stevema/laravel-form

```


### 使用说明

```php
# 1、生成配置文件
$ php artisan vendor:publish --tag="form"

# 2、修改配置文件 /config/form.php
AFS_SECRET=  # 加密用到的串
AFS_EXPIRE=  # 过期时间 默认600(秒)
AFS_SCENES=  # 场景-多个中间加逗号分隔 默认: 'nvc_login,nvc_register,slide_login,slide_register,click_login,click_register'
```

### 目录说明

```
├─ src
│   ├─ Config           # 配置文件目录
│   │   └─ config.php   # 配置文件
│   ├─ Facades          # 门面文件目录
│   │   └─ Form.php     # 门面
│   └─ AfsCrype.php     # 加密解密文件
│   └─ AfsException.php # 异常文件
│   └─ AfsManage.php    # 项目主文件
│   └─ AfsProvider.php  # 服务者
│   └─ Helper.php       # 帮助文件
└─ composer.json        # 配置

```

### 使用方式

```php
# 引用门面
use Stevema\Facades\Form;

# 随便加一个路由
Route::get('/t/afs', function(){
    /**
     * @method static name(?string $name='form')
     * @method float|array|int|string getValue()
     * @method static create(array|Closure|string|null $inputs)
     * @method static getInputs()
     * @method void setValues(array $values)
     * @method array getValues(bool $withGroup = false)
     * @method static arrCreate(array $arr)
     * @method static jsonCreate(string $json)
     * @method static input(string $type, string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static group(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static text(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static textarea(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static radio(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static checkbox(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static select(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static file(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static password(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static number(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static range(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static email(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static tel(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static birthday(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static date(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static datetime(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     * @method static time(string $key, ?string $title=null, array|Closure|string|null $childs=null)
     */
    // create 方法可以传入input数组 json 或者闭包 来自由组合
    // 数组和json 可以使用对应的方法 结果一样 arrCreate($arr); jsonCreate($json);
    $form = Form::create(function(){
        Form::group('group1', "简单分组",function(){
            Form::text('name1', "姓名1")->value("你们好");
            Form::tel('tel1', "手机号1")->value("17319333720");
            Form::group('group2', "简单分组2",function(){
                Form::select('select1', "select1")
                    ->options([["title"=>"选择1", "value" => 1, "checked"=>1]])->value(1);
                Form::tel('tel2', "手机号2");
                Form::checkbox('checkbox1', "checkbox1")
                    ->options([["title"=>"选择1", "value" => 1, "checked"=>1]])->value([1]);
            });
        });
        Form::text('name', "姓名");
        Form::tel('tel', "手机号");
    });
    $form = Form::create([
        ["type"=>"group", "name"=>"group1", "title"=>"简单分组", "childs"=>[
            ["type"=>"text", "name"=>"name1", "title"=>"姓名"],
            ["type"=>"text", "name"=>"name2", "title"=>"姓名2"],
            ["type"=>"text", "name"=>"name3", "title"=>"姓名3"],
        ]],
    ]);
    // 通过json来获取表单信息 - -  json可以存数据库 
    $form = Form::jsonCreate('[{"type":"group","name":"group1","title":"\u7b80\u5355\u5206\u7ec4","value":"","value_title":"","placeholder":null,"hidden":false,"required":false,"disabled":false,"desc":null,"unit":null,"help":null,"extra":null,"childs":[{"type":"text","name":"name1","title":"\u59d3\u540d1","value":"\u4f60\u4eec\u597d","value_title":"\u4f60\u4eec\u597d","placeholder":null,"hidden":false,"required":false,"disabled":false,"desc":null,"unit":null,"help":null,"extra":null,"childs":[]},{"type":"tel","name":"tel1","title":"\u624b\u673a\u53f71","value":"17319333720","value_title":"17319333720","placeholder":null,"hidden":false,"required":false,"disabled":false,"desc":null,"unit":null,"help":null,"extra":null,"childs":[]},{"type":"group","name":"group2","title":"\u7b80\u5355\u5206\u7ec42","value":"","value_title":"","placeholder":null,"hidden":false,"required":false,"disabled":false,"desc":null,"unit":null,"help":null,"extra":null,"childs":[{"type":"select","name":"select1","title":"select1","value":1,"value_title":"\u9009\u62e91","placeholder":null,"hidden":false,"required":false,"disabled":false,"desc":null,"unit":null,"help":null,"extra":null,"childs":[],"options":[{"title":"\u9009\u62e91","value":1,"checked":1}],"multiple":false},{"type":"tel","name":"tel2","title":"\u624b\u673a\u53f72","value":"123123333","value_title":"123123333","placeholder":null,"hidden":false,"required":false,"disabled":false,"desc":null,"unit":null,"help":null,"extra":null,"childs":[]},{"type":"checkbox","name":"checkbox1","title":"checkbox1","value":[1],"value_title":"\u9009\u62e91","placeholder":null,"hidden":false,"required":false,"disabled":false,"desc":null,"unit":null,"help":null,"extra":null,"childs":[],"options":[{"title":"\u9009\u62e91","value":1,"checked":1}],"multiple":true}]}]},{"type":"text","name":"name","title":"\u59d3\u540d","value":"aasss","value_title":"aasss","placeholder":null,"hidden":false,"required":false,"disabled":false,"desc":null,"unit":null,"help":null,"extra":null,"childs":[]},{"type":"tel","name":"tel","title":"\u624b\u673a\u53f7","value":"123123","value_title":"123123","placeholder":null,"hidden":false,"required":false,"disabled":false,"desc":null,"unit":null,"help":null,"extra":null,"childs":[]}]');
        
    # 第二种方式 来自 app()
//    $form = app('form');
//    $form->create();
    
    # 第三种方式  来自 helper 
//    $form = form();
//    $form->create();
    
    //每个input 都有自己的特殊属性 具体查看 \Stevema\Form\Types\InputBase::class Range还包括 min max
//    public function default(mixed $default): static;
//    public function placeholder(?string $placeholder): static;
//    public function hidden(bool $hidden): static;
//    public function required(bool $required): static;
//    public function disabled(bool $disabled): static;
//    public function desc(?string $desc): static;
//    public function unit(?string $unit): static;
//    public function help(?string $help): static;
//    public function extra(?array $extra): static;
//    public function appendExtra(string $key, mixed $value): static;
//    public function checkMultiple():bool;
//    public function isGroup(): bool;
//    public function hasChild(): bool;
//    public function getChilds(): array;
//    public function childs(array|Closure|string|null $childs): void;
//    public function getValue(): float|array|int|string;
//    public function value(string|int|float|array $value):static;
    
    // 表单可以设置值
    $form->setValues([
        'name1' => "你们好",
        'tel1' => "17319333720",
        'select1' => 1,
        'tel2' => "123123333",
        'tel' => "123123",
        "name" => "aasss"
    ]);
    $form->getValues(); //获取表单的所有的值 分组的话会过滤  ['name1' => 'value1',"name2" => "value2"]
    $form->getValues(true); //获取表单的所有的值 分组会组成数组  ['name1' => 'value1',"group"=>["name2" => "value2"]]
    
    //获取所有的input
    $form->getInputs();
    // 最终返回应该是
    return response()->json($form->getInputs());
    
    // 表单验证还没做 - laravel的表单验证挺好用的  也很搭这个form 
    $values = $form->getValues();
    // 验证也很简单
//    $request->validate($values, $rules, $messages);
    // 欢迎自己改造
});

```

### 备注

1. 接下来就是对数据库的存储了 - 添加map 对应字段和表单name
2. form添加save接口存储数据 - 每个表单需要编写自己的验证 
3. form 应该有设置input的options的相关方法 不是每个options 都是写死的 有些是可以变动的
4. 根据form_id和数据库来获取表单 - 存储数据 - 数据展示等等功能
5. 添加缓存-更快的展示表单 
6. 导出表单的数据-导出功能
