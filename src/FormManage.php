<?php
namespace Stevema\Form;

use Closure;
class FormManage extends FormCore {
    /**
     * @param array|Closure|string|null $inputs
     * @return $this
     */
    public function create(array|Closure|string|null $inputs):static
    {
        if (is_string($inputs)) {
            $this->jsonCreate($inputs);
        } else {
            $this->childs($inputs);
        }
        return $this;
    }

    /**
     * 获取inputs（childs）
     * @return array
     */
    public function getInputs(){
        return $this->getChilds();
    }

    /**
     * @param array $arr
     * @return $this
     */
    public function arrCreate(array $arr):static{
        return $this->create($arr);
    }

    /**
     * @param string $json
     * @return $this
     */
    public function jsonCreate(string $json):static{
        $arr = json_decode($json, true);
        return $this->arrCreate($arr);
    }

}
