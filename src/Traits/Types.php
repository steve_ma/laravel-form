<?php
namespace Stevema\Form\Traits;
use Closure;
use Stevema\Form\Types\InputBase;
trait Types
{
    public function input(string $type, string $key, ?string $title=null, array|Closure|string|null $childs=null){
        $input = $this->appendChild($this->getInputClass($type, $key, $title));
        $input->childs($childs);
        return $input;
    }
    /**
     * group 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function group(string $key, ?string $title=null, array|Closure|string|null $childs=null): InputBase
    {
        $group = $this->appendChild($this->getInputClass('group', $key, $title));
        $group->childs($childs);
        return $group;
    }

    /**
     * text 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function text(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('text', $key, $title));
        $input->childs($childs);
        return $input;
    }

    /**
     * textarea 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function textarea(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('textarea', $key, $title));
        $input->childs($childs);
        return $input;
    }

    /**
     * radio 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function radio(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('radio', $key, $title));
        $input->childs($childs);
        return $input;
    }

    /**
     * checkbox 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function checkbox(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('checkbox', $key, $title));
        $input->multiple(true);
        $input->childs($childs);
        return $input;
    }

    /**
     * select 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function select(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('select', $key, $title));
        $input->childs($childs);
        return $input;
    }

    /**
     * file 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function file(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('file', $key, $title));
        $input->childs($childs);
        return $input;
    }

    /**
     * password 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function password(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('password', $key, $title));
        $input->childs($childs);
        return $input;
    }

    /**
     * number 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function number(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('number', $key, $title));
        $input->childs($childs);
        return $input;
    }

    /**
     * range 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function range(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('range', $key, $title));
        $input->childs($childs);
        return $input;
    }

    /**
     * email 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function email(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('email', $key, $title));
        $input->childs($childs);
        return $input;
    }

    /**
     * tel 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function tel(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('tel', $key, $title));
        $input->childs($childs);
        return $input;
    }

    /**
     * birthday 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function birthday(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('birthday', $key, $title));
        $input->childs($childs);
        return $input;
    }

    /**
     * date 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function date(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('date', $key, $title));
        $input->childs($childs);
        return $input;
    }

    /**
     * datetime 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function datetime(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('datetime', $key, $title));
        $input->childs($childs);
        return $input;
    }

    /**
     * time 类型的input
     * @param string $key
     * @param string|null $title
     * @param array|Closure|string|null $childs
     * @return InputBase
     * @throws \Stevema\Form\FormException
     */
    public function time(string $key, ?string $title=null, array|Closure|string|null $childs=null):InputBase
    {
        $input = $this->appendChild($this->getInputClass('time', $key, $title));
        $input->childs($childs);
        return $input;
    }
}
