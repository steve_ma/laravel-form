<?php
namespace Stevema\Form\Traits;

trait Inputs
{
    /**
     * 子元素分组堆集合
     * @var array
     */
    protected $childStack = [];

    /**
     * 抛出最后的子元素分组堆
     * @return void
     */
    public function popChildStack(): void
    {
        array_pop($this->childStack);
    }

    /**
     * 在最后面添加子元素分组堆
     * @param $input
     * @return void
     */
    public function appendChildStack($input): void
    {
        $this->childStack[] = $input;
    }

    /**
     * 获取最后一个子元素分组堆
     * @return false|mixed
     */
    public function getLastChildStack()
    {
        return end($this->childStack);
    }

    /**
     * 判断有没有子元素分组堆
     * @return bool
     */
    public function hasChildStack(): bool
    {
        return ! empty($this->childStack);
    }
}
