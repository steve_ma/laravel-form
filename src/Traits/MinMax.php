<?php
namespace Stevema\Form\Traits;

trait MinMax
{
    /**
     * 最小值
     * @var int
     */
    public int $min = 0;
    /**
     * 最大值
     * @var int
     */
    public int $max = 5;

    /**
     * 设置最小值
     * @param Int $min
     * @return $this
     */
    public function min(Int $min): static
    {
        $this->min = $min;
        return $this;
    }

    /**
     * 设置最大值
     * @param Int $max
     * @return $this
     */
    public function max(Int $max): static
    {
        $this->max = $max;
        return $this;
    }
}
