<?php
namespace Stevema\Form\Traits;
use Illuminate\Support\Arr;
use Stevema\Facades\Form;

trait Values
{
    /**
     * 获取当前的值
     * @return float|array|int|string
     */
    public function getValue(): float|array|int|string
    {
        return $this->value;
    }

    /**
     * 批量设置
     * @param array $values
     * @param array $subvalues
     * @return void
     */
    public function setValues(array $values, array $subvalues=[]): void
    {
        $name = $this->name;

        $value = $values[$name] ?? ($subvalues[$name] ?? ($this->value ?? ($this->default ?? [])));
        if($this->hasChild()) {
            foreach (Arr::wrap($this->getChilds()) as $child) {
                $child->setValues($values, is_array($value) ? $value : []);
            }
        }

        if($this->isGroup()) {
            // 设置子元素分组
        } else {
//            dump($this->name, $value);
            $this->value($value);
        }

//        dump($this);
        $this->resetValueTitle();
    }

    /**
     * 批量获取值
     * @param bool $withGroup
     * @return float|array|int|string
     */
    public function getValues(bool $withGroup = false): float|array|int|string
    {
        $name = $this->name;
        $value = $this->getValue();
        if($withGroup){
            if($this->isGroup()) {
                // 设置子元素分组
                $obj = new \stdClass();
                $obj->name = $name;
                $obj->value = [];
                Form::appendChildStack($obj);
            } else {
                Form::appendValue($name, $value);
            }
        } else {
            if (!$this->isGroup()) {
                Form::appendValue($name, $value);
            }
        }
        if($this->hasChild()) {
            foreach (Arr::wrap($this->getChilds()) as $child) {
                $child->getValues($withGroup);
            }
        }
        if($withGroup) {
            if ($this->isGroup()) {
                // 找到父元素
                $parent = Form::getLastChildStack();
                //  父元素添加子元素到子元素集合中
                Form::appendValues($parent->name, $parent->value);
                // 移除子元素分组
                Form::popChildStack();
            }
        }
        return Form::getValue();
    }

    /**
     * 重置valueTitle
     * @return void
     */
    public function resetValueTitle(){
        if(empty($this->value)){
            $this->value = $this->default ?? [];
        }
        if(!is_array($this->value)) {
            $this->value_title = strval($this->value);
        }
        if(isset($this->options) && $this->options){
            $title_arr = [];
            foreach($this->options as $k => $v){
                $value_check = False;
                if($this->isMultiple()){
                    $value_check = in_array($v['value'], $this->value);
//                    $value_check = true;
                } else {
                    $value_check = $this->value == $v['value'];
                }
                if($value_check){
                    $title_arr[] = $v['title'];
                    $childs = $v['childs'] ?? null;
                    if($childs){
                        foreach($childs as $child2){
                            $child2->getValue();
                        }
                    }
                }
            }
            $this->value_title = implode("\r\n", $title_arr);
        }
    }

    /**
     * 添加进values中 这里用到大部分 Childs 的方法
     * @param $key
     * @param $value
     * @return void
     */
    public function appendValue($key, $value):void
    {
        // 判断有没有相关的父元素
        if(Form::hasChildStack()){
            // 找到父元素
            $parent = Form::getLastChildStack();
            //  父元素添加子元素到子元素集合中
            $parent->value[$key] = $value;
        } else {
            Form::appendValues($key, $value);
        }
    }

}
