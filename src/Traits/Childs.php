<?php
namespace Stevema\Form\Traits;
use Closure;
use Illuminate\Support\Arr;
use Stevema\Form\FormException;
use Stevema\Form\Types\InputBase;
use Stevema\Facades\Form;

trait Childs
{
    /**
     * 子元素集合
     * @var array
     */
    public array $childs = [];

    /**
     * 有没有子元素
     * @return bool
     */
    public function hasChild(): bool
    {
        return ! empty($this->childs);
    }

    /**
     * 获取子元素
     * @return array
     */
    public function getChilds(): array
    {
        return $this->childs;
    }

    /**
     * 批量设置子元素
     * @param array|Closure|string|null $childs
     * @return void
     */
    public function childs(array|Closure|string|null $childs): void
    {
        if(! empty($childs)) {
            foreach (Arr::wrap($childs) as $child) {
                // 设置子元素分组
                Form::appendChildStack($this);
                // 设置子元素
                Form::loadInputs($child);
                // 移除子元素分组
                Form::popChildStack();
            }
        }
    }

    /**
     * 设置子元素
     * @param $child
     * @return void
     * @throws FormException
     */
    public function loadInputs($child): void
    {
        if ($child instanceof Closure) {
            $child($this);
        } else {
            //其它
            $this->addChild($child);
        }
    }

    /**
     * 通过数组设置子元素
     * @param array $arr
     * @return InputBase
     * @throws FormException
     */
    public function addChild(array $arr):InputBase {
        // 数组的话 必须包含 type key title可以是空
        if(empty($arr['type']) || empty($arr['name']))
            throw new FormException("type、name 必须存在");
        // 添加进子元素集合
        $input = $this->appendChild($this->getInputClass($arr['type'], $arr['name'], $arr['title']));

        foreach($arr as $k => $v){
            if(!in_array($k, ['type','name','title','childs','value_title'])) {
                if (method_exists($input, $k)) {
                    if(!empty($v)) {
                        $input->{$k}($v);
                    }
                } else {
                    if(!isset($input->{$k})) {
                        $input->appendExtra($k, $v);
                    }
                }
            }
        }
        $input->resetValueTitle();
        // 这个子元素 有没有自己的子元素
        $childs = $arr['childs'] ?? [];
        if(!empty($childs)) {
            // 批量设置子元素
            $input->childs($childs);
        }

        return $input;
    }

    /**
     * 获取元素的实例
     * @param string $type
     * @param string $name
     * @param string|null $title
     * @return InputBase
     * @throws FormException
     */
    public function getInputClass(string $type, string $name, ?string $title):InputBase{
        // 获取配置信息
        $config = config('form');
        // 获取类型map
        $maps = $config['types_class_maps'];
        // 从map中查找相关类型
        if(!isset($maps[$type])) throw new FormException("{$type} 类型不存在,请到configs中添加相关类型");
        // map中定义的类型是否存在实例的可能
        if(!class_exists($maps[$type])) throw new FormException("{$maps[$type]} 类不存在");
        // 返回实例信息
        return new $maps[$type]($type, $name, $title);
    }

    /**
     * 把input实例添加进子元素集合
     * @param InputBase $input
     * @return InputBase
     * @throws FormException
     */
    public function appendChild(InputBase $input): InputBase
    {
        // 元素必须继承 \Stevema\Form\Types\InputBase
        $res = $input instanceof InputBase;
        if(!$res) {
            throw new FormException("input 必须继承自 \Stevema\Form\Types\InputBase");
        }
        // 判断有没有相关的父元素
        if(Form::hasChildStack()){
            // 找到父元素
            $parent = Form::getLastChildStack();
            //  父元素添加子元素到子元素集合中
            $parent->childs[] = $input;
        }
        // 返回当前的元素
        return $input;
    }

}
