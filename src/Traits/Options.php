<?php
namespace Stevema\Form\Traits;

trait Options
{
    /**
     * select  radio checkbox 等都需要的 一般是 二维数组 包含 key value
     * @var array
     */
    public array $options = []; // select  radio checkbox 等都需要的 一般是 二维数组 包含 title value checked

    /**
     * 设置 checkbox
     * @param array $options
     * @return $this
     */
    public function options(array $options): static
    {
        $this->options = $options;
        return $this;
    }


    /**
     * 多选  true 是多选 false 是单选
     * @var bool
     */
    public bool $multiple = false;

    /**
     * 设置多选
     * @param bool $multiple
     * @return $this
     */
    public function multiple(bool $multiple): static
    {
        $this->multiple = $multiple;
        if($multiple){
            if(!is_array($this->value)){
                $this->value = [$this->value];
            }
        } else {
            if(is_array($this->value)){
                $this->value = $this->value[0] ?? [];
            }
        }
        return $this;
    }

    /**
     * 判断是否多选
     * @return bool
     */
    public function isMultiple(): bool
    {
        return $this->multiple;
    }
}
