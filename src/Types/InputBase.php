<?php
namespace Stevema\Form\Types;

use Stevema\Form\Interfaces\InputInterface;
use Stevema\Form\Traits\Childs;
use Stevema\Form\Traits\Values;
use Stevema\Facades\Form;

/**
 * @method multiple(bool $multiple = false)
 */
class InputBase implements InputInterface
{
    /**
     * 子元素
     */
    use Childs, Values;

    /**
     * 类型  group text  select 等
     * @var string
     */
    public string $type = "";

    /**
     * name
     * @var string
     */
    public string $name = "";

    /**
     * 标题-显示在input前面的
     * @var string|null
     */
    public ?string $title = null;

    /**
     * construct
     * @param string $type
     * @param string $name
     * @param string|null $title
     */
    public function __construct(string $type, string $name, ?string $title=null){
        $this->type = $type;
        $this->name = $name;
        $this->title = $title;
    }

    /**
     * 值
     * @var string|int|float|array
     */
    public string|int|float|array $value = "";

    /**
     * 判断是不是group类型的input
     * @return bool
     */
    public function isGroup(): bool
    {
        return $this->type === 'group';
    }

    /**
     * 设置值
     * @param string|int|float|array $value
     * @return $this
     */
    public function value(string|int|float|array $value):static
    {
        $this->value = $value;
        $this->resetValueTitle();
        return $this;
    }

    /**
     * 默认值
     * @var mixed|null
     */
    private mixed $default = '';

    /**
     * 设置默认值
     * @param mixed $default
     * @return $this
     */
    public function default(mixed $default): static
    {
        $this->default = $default;
        return $this;
    }

    /**
     * 值-显示使用 比如 radio 的option 可能键和值不一致 就需要这个来获取 键对应的值
     * @var string|null
     */
    public ?string $value_title = null;

    /**
     * // 默认文本
     * @var string|null
     */
    public ?string $placeholder = null;

    /**
     * 设置默认文本
     * @param string|null $placeholder
     * @return $this
     */
    public function placeholder(?string $placeholder): static
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * 是否隐藏
     * @var bool
     */
    public bool $hidden = false;

    /**
     * 设置是否隐藏
     * @param bool $hidden
     * @return $this
     */
    public function hidden(bool $hidden): static
    {
        $this->hidden = $hidden;
        return $this;
    }

    /**
     * 是否必须
     * @var bool
     */
    public bool $required = false;

    /**
     * 设置是否必须
     * @param bool $required
     * @return $this
     */
    public function required(bool $required): static
    {
        $this->required = $required;
        return $this;
    }

    /**
     * 是否禁用
     * @var bool
     */
    public bool $disabled = false;

    /**
     * 设置是否禁用
     * @param bool $disabled
     * @return $this
     */
    public function disabled(bool $disabled): static
    {
        $this->disabled = $disabled;
        return $this;
    }

    /**
     * 说明  input下面对这个字段的说明
     * @var string|null
     */
    public ?string $desc = null;

    /**
     * 设置说明
     * @param string|null $desc
     * @return $this
     */
    public function desc(?string $desc): static
    {
        $this->desc = $desc;
        return $this;
    }

    /**
     * 单位  input后面显示的单位  如  公里  mm 等
     * @var string|null
     */
    public ?string $unit = null;

    /**
     * 设置单位
     * @param string|null $unit
     * @return $this
     */
    public function unit(?string $unit): static
    {
        $this->unit = $unit;
        return $this;
    }

    /**
     * 帮助 - - input后面可能会有小问号  鼠标移动上去显示文本
     * @var string|null
     */
    public ?string $help = null;

    /**
     * 设置帮助
     * @param string|null $help
     * @return $this
     */
    public function help(?string $help): static
    {
        $this->help = $help;
        return $this;
    }

    /**
     * 其它属性  比如 class  script  data-if 等
     * @var array|null
     */
    public ?array $extra = null;  // 其它

    /**
     * 设置其它属性
     * @param array|null $extra
     * @return $this
     */
    public function extra(?array $extra): static
    {
        $this->extra = $extra;
        return $this;
    }

    /**
     * append其他属性
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function appendExtra(string $key, mixed $value): static
    {
        $this->extra[$key] = $value;
        return $this;
    }

    /**
     * 检测是否多选
     * @return bool
     */
    public function checkMultiple():bool{
        return $this->multiple ?? false;
    }
}
