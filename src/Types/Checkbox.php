<?php
namespace Stevema\Form\Types;
use Stevema\Form\Traits\Options;
use Stevema\Form\Traits\Multiple;
class Checkbox extends InputBase {
    // 有options
    use Options;
    /**
     * 类型  group text  select 等
     * @var string
     */
    public string $type = "checkbox";

    /**
     * name
     * @var string
     */
    public string $name = "";

    /**
     * 标题-显示在input前面的
     * @var string|null
     */
    public ?string $title = null;

    /**
     * 值
     * @var string|int|float|array
     */
    public string|int|float|array $value = [];

    /**
     * 默认值
     * @var mixed|null
     */
    private mixed $default = "";

    /**
     * 值-显示使用 比如 radio 的option 可能键和值不一致 就需要这个来获取 键对应的值
     * @var string|null
     */
    public ?string $value_title = null;

    /**
     * // 默认文本
     * @var string|null
     */
    public ?string $placeholder = null;

    /**
     * 是否隐藏
     * @var bool
     */
    public bool $hidden = false;

    /**
     * 是否必须
     * @var bool
     */
    public bool $required = false;

    /**
     * 是否禁用
     * @var bool
     */
    public bool $disabled = false;

    /**
     * 说明  input下面对这个字段的说明
     * @var string|null
     */
    public ?string $desc = null;

    /**
     * 单位  input后面显示的单位  如  公里  mm 等
     * @var string|null
     */
    public ?string $unit = null;

    /**
     * 帮助 - - input后面可能会有小问号  鼠标移动上去显示文本
     * @var string|null
     */
    public ?string $help = null;

    /**
     * 其它属性  比如 class  script  data-if 等
     * @var array|null
     */
    public ?array $extra = null;  // 其它
}
