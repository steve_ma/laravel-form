<?php
namespace Stevema\Form\Types;

class Group extends InputBase {
    /**
     * 类型  group text  select 等
     * @var string
     */
    public string $type = "group";

    /**
     * name
     * @var string
     */
    public string $name = "";

    /**
     * 标题-显示在input前面的
     * @var string|null
     */
    public ?string $title = null;

    /**
     * 值
     * @var string|int|float|array
     */
    public string|int|float|array $value = [];
}
