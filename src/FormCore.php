<?php
namespace Stevema\Form;

use Stevema\Form\Interfaces\FormInterface;
use Stevema\Form\Traits\Types;
use Stevema\Form\Traits\Inputs;
use Stevema\Form\Traits\Childs;
use Stevema\Form\Traits\Values;
class FormCore implements FormInterface {
    /**
     * 添加子元素相关  子元素分组堆相关  类型相关
     */
    use Childs,Inputs,Types,Values;

    /**
     * 表单名
     * @var string|null
     */
    public ?string $name = "form";

    /**
     * 设置表单名
     * @param string|null $name
     * @return $this
     */
    public function name(?string $name='form'): static
    {
        $this->name = $name??'form';
        return $this;
    }

    // value 集合
    protected array $value = [];
    private string $value_title = "";

    public function isGroup(): bool
    {
        return true;
    }
    public function __construct(){

    }

    /**
     * append 值
     * @param $name
     * @param $value
     * @return void
     */
    public function appendValues($name, $value):void{
        $this->value[$name] = $value;
    }

}
