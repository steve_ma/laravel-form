<?php
use Stevema\Form\Types\Group;
use Stevema\Form\Types\Text;
use Stevema\Form\Types\Textarea;
use Stevema\Form\Types\Radio;
use Stevema\Form\Types\Checkbox;
use Stevema\Form\Types\Select;
use Stevema\Form\Types\File;
use Stevema\Form\Types\Password;
use Stevema\Form\Types\Number;
use Stevema\Form\Types\Range;
use Stevema\Form\Types\Email;
use Stevema\Form\Types\Tel;
use Stevema\Form\Types\Birthday;
use Stevema\Form\Types\Date;
use Stevema\Form\Types\DateTime;
use Stevema\Form\Types\Time;
return [
    /*
    |--------------------------------------------------------------------------
    | Form Config
    |--------------------------------------------------------------------------
    |
    | form
    |
    */
    // 类型对应的类关系表
    "types_class_maps" => [
        "group" => Group::class,
        "text" => Text::class,
        "textarea" => Textarea::class,
        "radio" => Radio::class,
        "checkbox" => Checkbox::class,
        "select" => Select::class,
        "file" => File::class,
        "password" => Password::class,
        "number" => Number::class,
        "range" => Range::class,
        "email" => Email::class,
        "tel" => Tel::class,
        "birthday" => Birthday::class,
        "date" => Date::class,
        "datetime" => DateTime::class,
        "time" => Time::class,
    ]
];
