<?php
namespace Stevema\Form\Interfaces;
use Closure;
interface FormInterface {

    public function isGroup(): bool;
    public function hasChild(): bool;
    public function getChilds(): array;
    public function childs(array|Closure|string|null $childs): void;

    public function setValues(array $values, array $subvalues=[]): void;
    public function getValues(bool $withGroup = false): float|array|int|string;
    public function appendValues($name, $value):void;
}
