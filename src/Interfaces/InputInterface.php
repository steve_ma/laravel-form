<?php
namespace Stevema\Form\Interfaces;

use Closure;
interface InputInterface {
    public function default(mixed $default): static;
    public function placeholder(?string $placeholder): static;
    public function hidden(bool $hidden): static;
    public function required(bool $required): static;
    public function disabled(bool $disabled): static;
    public function desc(?string $desc): static;
    public function unit(?string $unit): static;
    public function help(?string $help): static;
    public function extra(?array $extra): static;
    public function appendExtra(string $key, mixed $value): static;
    public function checkMultiple():bool;
    public function isGroup(): bool;
    public function hasChild(): bool;
    public function getChilds(): array;
    public function childs(array|Closure|string|null $childs): void;

    public function getValue(): float|array|int|string;
    public function value(string|int|float|array $value):static;

}
