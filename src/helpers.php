<?php

use Stevema\Form\FormManage;
use Stevema\Facades\Form;
if(! function_exists('form')){
    function form(){
        if(function_exists('app')) {
            return app('form');
        }
        $config_file_path = realpath(__DIR__.'/../Config/config.php');
        $config = require($config_file_path);
        return new FormManage($config);
    }
}
