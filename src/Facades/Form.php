<?php
namespace Stevema\Facades;

use Illuminate\Support\Facades\Facade;
use Closure;
/**
 * @method static name(?string $name='form')
 * @method float|array|int|string getValue()
 * @method static create(array|Closure|string|null $inputs)
 * @method static getInputs()
 * @method void setValues(array $values)
 * @method array getValues(bool $withGroup = false)
 * @method static arrCreate(array $arr)
 * @method static jsonCreate(string $json)
 * @method static input(string $type, string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static group(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static text(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static textarea(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static radio(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static checkbox(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static select(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static file(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static password(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static number(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static range(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static email(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static tel(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static birthday(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static date(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static datetime(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 * @method static time(string $key, ?string $title=null, array|Closure|string|null $childs=null)
 */
class Form extends Facade
{
    /**
     * Get the name of the class registered in the Application container.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'form';
    }
}
